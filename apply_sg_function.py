import json
import logging
import os
import signal
import boto3
from urllib import request

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("Received event: " + json.dumps(event, indent=2))
    signal.alarm(int(context.get_remaining_time_in_millis() / 1000) - 1)

    try:
        request_type = event["RequestType"]

        instance_ids = os.environ["COMPROMISED_EC2_INSTANCE_IDS"].split(",")
        isolated_security_group_id = os.environ["ISOLATED_SECURITY_GROUP_ID"]
        table_name = os.environ["SECURITY_GROUP_TABLE_NAME"]

        LOGGER.info(request_type)

        if request_type == "Create":
            isolate_ec2_instances_by_replacing_security_groups(instance_ids, isolated_security_group_id, table_name)
            send_success(event, context, "Resource created successfully")
        elif request_type == "Update":
            isolate_ec2_instances_by_replacing_security_groups(instance_ids, isolated_security_group_id, table_name)
            send_success(event, context, "Resource updated successfully")
        elif request_type == "Delete":
            restore_security_group_for_ec2_instance(instance_ids, table_name)
            send_success(event, context, "Resource deleted successfully")
        else:
            send_failure(event, context, "Unexpected event received from CloudFormation")
    except Exception as e:
        send_failure(event, context, str(e))


def isolate_ec2_instances_by_replacing_security_groups(instance_ids, security_group_id, table_name):
    ec2 = boto3.resource('ec2')
    dynamodb = boto3.resource("dynamodb")

    instances = ec2.instances.filter(InstanceIds=instance_ids)
    table = dynamodb.Table(table_name)

    for instance in instances:
        store_security_group_ids_in_table(instance, table)
        replace_security_groups_for_instance(instance, [security_group_id])


def store_security_group_ids_in_table(instance, table):
    item = {
        "InstanceId": instance.instance_id,
        "SecurityGroupIds": [group['GroupId'] for group in instance.security_groups]
    }

    table.put_item(Item=item)


def replace_security_groups_for_instance(ec2_instance, security_groups):
    ec2_instance.modify_attribute(Groups=security_groups)


def restore_security_group_for_ec2_instance(instance_ids, table_name):
    LOGGER.info("Restoring security groups for instances " + str(instance_ids))

    ec2 = boto3.resource('ec2')
    dynamodb = boto3.resource("dynamodb")

    instances = ec2.instances.filter(InstanceIds=instance_ids)
    table = dynamodb.Table(table_name)

    for instance in instances:
        key = {"InstanceId": instance.instance_id}
        item = table.get_item(Key=key)
        security_group_ids = item["Item"]["SecurityGroupIds"]

        LOGGER.debug("Security groups found: " + str(security_group_ids))

        replace_security_groups_for_instance(instance, security_group_ids)


def send_success(event, context, message):
    LOGGER.info(message)

    send_response(event, context, "SUCCESS", {
        "Message": message
    })


def send_failure(event, context, message):
    LOGGER.error(message)

    send_response(event, context, "FAILED", {
        "Message": message
    })


def send_response(event, context, response_status, response_data):
    response_body = json.dumps({
        "Status": response_status,
        "Reason": "Details in CloudWatch Logstream: " + context.log_stream_name,
        "PhysicalResourceId": context.log_stream_name,
        "StackId": event["StackId"],
        "RequestId": event["RequestId"],
        "LogicalResourceId": event["LogicalResourceId"],
        "Data": response_data
    })

    LOGGER.debug("ResponseURL: %s", event["ResponseURL"])
    LOGGER.debug("ResponseBody: %s", response_body)

    data = response_body.encode('utf-8')
    req = request.Request(
        event['ResponseURL'],
        data=data,
        headers={'Content-Length': len(data), 'Content-Type': ''}
    )
    req.get_method = lambda: 'PUT'

    with request.urlopen(req) as response:
        LOGGER.info(response.read())


def timeout_handler(signal, frame):
    raise Exception('Lambda timeout')


signal.signal(signal.SIGALRM, timeout_handler)
