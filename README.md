# Automated EC2 incident response
This project attempts to speed up the response stage during an incident response for compromised EC2 instances. The 
cloudformation template provisions an isolated security group that is only accessible by SSH from a forensic security
 group. An instance is deployed into the forensic security group and bootstrapped with SIFT. The compromised EC2 
 instances are stripped of their security groups and placed in the isolated security group for forensic analysis.
 
On deletion of the stack the old security groups for each instance are restored.

## Deployment instructions
`aws cloudformation package --template-file template.yml --output-template-file packaged_template.yml --s3-bucket 
<artifact_bucket_name> --region <aws_region>`

`aws cloudformation deploy --template-file packaged.yml --stack-name <stack_name> --region <aws_region> 
--capabilities CAPABILITY_IAM --parameter-overrides "VpcId=<vpc_id>" "Ec2KeyName=<existing_ec2_keypair_name>" 
"CompromisedInstanceList=<id_1>,<id_2>" "CompanyIp=<company_ip>"`